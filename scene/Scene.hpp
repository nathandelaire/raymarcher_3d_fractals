//
// Created by natha on 16/04/2021.
//

#ifndef RAYMARCHING_SCENE_HPP
#define RAYMARCHING_SCENE_HPP

#include <glm/vec3.hpp>

enum OBJ_TYPE{
    MANDELBULB = 0,
    MANDELBOX,
    PLANETS,
    GYROID,
    SPONGE
};

class Scene {
public:
    Scene(OBJ_TYPE object, const glm::vec3 &color, const glm::vec3 &position, float maxRaySteps, float minDistance,
          float maxDistance, bool renderGlow, bool renderAo, bool renderNormals, bool animate);

    OBJ_TYPE getObject() const;

    const glm::vec3 &getColor() const;

    const glm::vec3 &getPosition() const;

    float getMaxRaySteps() const;

    float getMinDistance() const;

    float getMaxDistance() const;

    bool isRenderGlow() const;

    bool isRenderAo() const;

    bool isRenderNormals() const;

    bool isAnimate() const;

private:
    OBJ_TYPE object;
    glm::vec3 color;
    glm::vec3 position;
    float MAX_RAY_STEPS;
    float MIN_DISTANCE;
    float MAX_DISTANCE;

    bool renderGlow;
    bool renderAO;
    bool renderNormals;

    bool animate;
};



#endif //RAYMARCHING_SCENE_HPP

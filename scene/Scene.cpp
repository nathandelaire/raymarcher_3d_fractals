//
// Created by natha on 16/04/2021.
//

#include "Scene.hpp"

Scene::Scene(OBJ_TYPE object, const glm::vec3 &color, const glm::vec3 &position, float maxRaySteps, float minDistance,
             float maxDistance, bool renderGlow, bool renderAo, bool renderNormals, bool animate) : object(object), color(color),
                                                                                      position(position),
                                                                                      MAX_RAY_STEPS(maxRaySteps),
                                                                                      MIN_DISTANCE(minDistance),
                                                                                      MAX_DISTANCE(maxDistance),
                                                                                      renderGlow(renderGlow),
                                                                                      renderAO(renderAo),
                                                                                      renderNormals(renderNormals),
                                                                                      animate(animate){}

OBJ_TYPE Scene::getObject() const {
    return object;
}

const glm::vec3 &Scene::getColor() const {
    return color;
}

const glm::vec3 &Scene::getPosition() const {
    return position;
}

float Scene::getMaxRaySteps() const {
    return MAX_RAY_STEPS;
}

float Scene::getMinDistance() const {
    return MIN_DISTANCE;
}

float Scene::getMaxDistance() const {
    return MAX_DISTANCE;
}

bool Scene::isRenderGlow() const {
    return renderGlow;
}

bool Scene::isRenderAo() const {
    return renderAO;
}

bool Scene::isRenderNormals() const {
    return renderNormals;
}

bool Scene::isAnimate() const {
    return animate;
}

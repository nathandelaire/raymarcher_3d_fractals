//
// Created by natha on 09/04/2021.
//

#include "Camera.hpp"

Camera::Camera() {}

Camera::Camera(glm::vec3 position,
               glm::vec3 direction,
               glm::vec3 worldUp) {

    this->worldUp = worldUp;
    this->camUp = worldUp;
    this->position = position;


    ViewMatrix = glm::mat4(1.f);
    movement_speed = 3.f;
    sensitivity = 50.f;
    right = glm::vec3(0.f);
    pitch = 0.f;
    yaw = -90.f;
    roll = 0.f;

    updateCameraVectors();

}

Camera::~Camera() {

}

void Camera::updateKeyboardInput(const float &dt, const int direction) {
    switch (direction) {
        case FORWARD:
            position += front * movement_speed * dt;
            break;
        case BACKWARD:
            position -= front * movement_speed * dt;
            break;
        case LEFT:
            position -= right * movement_speed * dt;
            break;
        case RIGHT:
            position += right * movement_speed * dt;
            break;
        case UP:
            position += worldUp * movement_speed * dt;
            break;
        case DOWN:
            position -= worldUp * movement_speed * dt;
            break;
        default:
            break;
    }
}

void Camera::updateMouseInput(const float &dt, const double &offsetX, const double &offsetY) {

    pitch += static_cast<GLfloat>(offsetY) * sensitivity * dt * -1;
    yaw += static_cast<GLfloat>(offsetX) * sensitivity * dt;

    //La rotation sur Y ne doit pas dépasser 80 (on ne peut pas tourner à l'infini)
    if(pitch >= 80.f)
        pitch = 80.f;
    if(pitch <= -80.f)
        pitch = -80.f;

    //La même pour X, on module autour de 360 degrés
    if(yaw > 360 || yaw < -360)
        yaw = 0.f;

}


void Camera::updateInput(const float &dt, const int direction, const double &offsetX, const double &offsetY) {
    updateKeyboardInput(dt, direction);
    updateMouseInput(dt, offsetX, offsetY);
}

void Camera::updateCameraVectors() {
    front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    front.y = sin(glm::radians(pitch));
    front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));

    //Toujours normalizer
    front = glm::normalize(front);
    right = glm::normalize(glm::cross(front, worldUp));
    camUp = glm::normalize(glm::cross(right, front));
}


glm::mat4 Camera::getViewMatrix(){

    updateCameraVectors();

    ViewMatrix = glm::lookAt(position, position + front, camUp);

    return ViewMatrix;
}

const glm::vec3 &Camera::getPosition() const {
    return position;
}

void Camera::setMovementSpeed(GLfloat movementSpeed) {
    movement_speed = movementSpeed;
}

const glm::vec3 &Camera::getFront() const {
    return front;
}

const glm::vec3 &Camera::getCamUp() const {
    return camUp;
}

const glm::vec3 &Camera::getRight() const {
    return right;
}

void Camera::setPosition(const glm::vec3 &position) {
    Camera::position = position;
}

void Camera::setFront(const glm::vec3 &front) {
    Camera::front = front;
}

void Camera::setCamUp(const glm::vec3 &camUp) {
    Camera::camUp = camUp;
}

void Camera::setRight(const glm::vec3 &right) {
    Camera::right = right;
}


//
// Created by natha on 07/04/2021.
//

#include "Mesh.hpp"

Mesh::Mesh(Primitive *shape){

    this->position = shape->getCenter();
    this->rotation = glm::vec3(0.f);
    this->scale = glm::vec3(1.f);

    initVAO(shape);
    updateModelMatrix();
}

Mesh::~Mesh() {
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
}

void Mesh::initVAO(Primitive *shape) {
    this->nbVertices = shape->getNbVertices();
    this->nbIndices = shape->getNbIndices();

    //VAO (Vertex Array Object), VBO (Vertex Buffer Objet), EBO (Element Buffer Object)
    //GEN VAO AND BIND
    //We want to create one -> 1
    glCreateVertexArrays(1, &VAO);
    //Everything will be in this box
    glBindVertexArray(VAO);

    //GEN VBO AND BIND AND SEND DATA
    //Send vertex data to GPU -> VBO
    glGenBuffers(1, &VBO);
    //We bind VBO to the OpenGL GL_ARRAY_BUFFER
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    //Data we send to GPU
    glBufferData(GL_ARRAY_BUFFER, shape->getNbVertices() * sizeof(Vertex), shape->getVertices(), GL_STATIC_DRAW);

    //GEN ENO AND BIND AND SEND DATA
    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, shape->getNbIndices() * sizeof(GLuint), shape->getIndices(),
                     GL_STATIC_DRAW);
    //SET VERTEXATTRIBPOINTERS AND ENABLE
    //Input assembly : Tell the graphic card how he's gonna use our data
    //attribLoc -> The first one (location = 0) in vertex core glsl program
    //3 -> 3 floats
    //GL_FLOAT -> 3 GL_FLOATS for first attribute
    //GL_FALSE -> Normalized or not ?
    //sizeof(Vertex) -> How long to we have to go to get to the next vertex ?
    //(GLvoid*)offsetof(Vertex, position) -> How much memory to the next attribute

    //Position
    //GLuint positionLoc = core_shader.getLocation("vertex_position");
    //0 = positionLoc
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position)); //position is in struct Vertex
    glEnableVertexAttribArray(0);

    //BIND VAO 0 (unbind just in case before rebinding in main loop)
    glBindVertexArray(0);
}


void Mesh::updateUniforms(Shader *shader) {
    shader->setMatrix4fx(ModelMatrix, "ModelMatrix");
}


void Mesh::updateModelMatrix() {
    //Generate Matrices
    ModelMatrix = glm::mat4(1.f); //Identity Matrix for translations, rotations and scaling
    //Scale -> Rotations -> Translations inside
    ModelMatrix = glm::translate(ModelMatrix, position);
    ModelMatrix = glm::rotate(ModelMatrix, rotation.x, glm::vec3(1.f, 0.f, 0.f)); //Rotation x
    ModelMatrix = glm::rotate(ModelMatrix, rotation.y, glm::vec3(0.f, 1.f, 0.f)); //Rotation y
    ModelMatrix = glm::rotate(ModelMatrix, rotation.z, glm::vec3(0.f, 0.f, 1.f)); //Rotation z
    ModelMatrix = glm::scale(ModelMatrix, scale);
}


void Mesh::render(Shader *shader) {
    //Update ModelMatrix in CPU and in GPU
    updateModelMatrix();
    updateUniforms(shader);

    shader->use();

    //Bind VAO
    glBindVertexArray(VAO);
    //Draw indices (triangles, nr of indices, type of indices, where do we start)
    if(nbIndices == 0)
        glDrawArrays(GL_TRIANGLES, 0, nbVertices);
    else
        glDrawElements(GL_TRIANGLES, nbIndices, GL_UNSIGNED_INT, 0);
}

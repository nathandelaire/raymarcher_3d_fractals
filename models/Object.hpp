//
// Created by natha on 14/04/2021.
//

#ifndef RAYMARCHING_OBJECT_HPP
#define RAYMARCHING_OBJECT_HPP

#include <glm/vec3.hpp>

class Object {
public:
    virtual ~Object() = default;

    virtual glm::vec3 getPosition() = 0;
    virtual float getSize() = 0;
    virtual int getType() = 0;
    virtual void setPosition(const glm::vec3 &position) = 0;

    //Just for mandlebulb
    virtual float getIterations() = 0;
    virtual float getBailout() = 0;
    virtual float getPower() = 0;
    virtual void setIterations(float iterations) = 0;
    virtual void setBailout(float bailout) = 0;
    virtual void setPower(float power) = 0;

    //Just for gyroid

    virtual float getScale() = 0;
    virtual float getThickness() = 0;
    virtual float getBiais() = 0;
    virtual void setScale(float scale) = 0;
    virtual void setThickness(float thickness) = 0;
    virtual void setBiais(float biais) = 0;


protected:
    glm::vec3 position;
    float size;

    //Just for mandlebulb
    float iterations;
    float bailout;
    float power;

    //Just for gyroid and mandelbox
    float scale;

    //Just for gyroid
    float thickness;
    float biais;
};

class Sphere : public Object {
public:
    Sphere(glm::vec3 position, float size) : Object()
    {
        this->position = position;
        this->size = size;
    }

    glm::vec3 getPosition() override {
        return position;
    }

    float getSize() override {
        return size;
    }

    int getType() override {
        return 0;
    }

    void setPosition(const glm::vec3 &position) override {
        this->position = position;
    }

    float getIterations() override {
        return 0;
    }

    float getBailout() override {
        return 0;
    }

    float getPower() override {
        return 0;
    }

    void setIterations(float iterations) override {

    }

    void setBailout(float bailout) override {

    }

    void setPower(float power) override {

    }

    float getScale() override {
        return 0;
    }

    float getThickness() override {
        return 0;
    }

    float getBiais() override {
        return 0;
    }

    void setScale(float scale) override {

    }

    void setThickness(float thickness) override {

    }

    void setBiais(float biais) override {

    }
};

class Cube : public Object {
public:
    Cube(glm::vec3 position, float size) : Object()
    {
        this->position = position;
        this->size = size;
    }

    glm::vec3 getPosition() override {
        return position;
    }

    float getSize() override {
        return size;
    }

    int getType() override {
        return 1;
    }

    void setPosition(const glm::vec3 &position) override {
        this->position = position;
    }

    float getIterations() override {
        return 0;
    }

    float getBailout() override {
        return 0;
    }

    float getPower() override {
        return 0;
    }

    void setIterations(float iterations) override {

    }

    void setBailout(float bailout) override {

    }

    void setPower(float power) override {

    }

    float getScale() override {
        return 0;
    }

    float getThickness() override {
        return 0;
    }

    float getBiais() override {
        return 0;
    }

    void setScale(float scale) override {

    }

    void setThickness(float thickness) override {

    }

    void setBiais(float biais) override {

    }
};

class Torus : public Object {
public:
    Torus(glm::vec3 position, float size) : Object()
    {
        this->position = position;
        this->size = size;
    }

    glm::vec3 getPosition() override {
        return position;
    }

    float getSize() override {
        return size;
    }

    int getType() override {
        return 2;
    }

    void setPosition(const glm::vec3 &position) override {
        this->position = position;
    }

    float getIterations() override {
        return 0;
    }

    float getBailout() override {
        return 0;
    }

    float getPower() override {
        return 0;
    }

    void setIterations(float iterations) override {

    }

    void setBailout(float bailout) override {

    }

    void setPower(float power) override {

    }

    float getScale() override {
        return 0;
    }

    float getThickness() override {
        return 0;
    }

    float getBiais() override {
        return 0;
    }

    void setScale(float scale) override {

    }

    void setThickness(float thickness) override {

    }

    void setBiais(float biais) override {

    }
};

class Octahedron : public Object {
public:
    Octahedron(glm::vec3 position, float size) : Object()
    {
        this->position = position;
        this->size = size;
    }

    glm::vec3 getPosition() override {
        return position;
    }

    float getSize() override {
        return size;
    }

    int getType() override {
        return 3;
    }

    void setPosition(const glm::vec3 &position) override {
        this->position = position;
    }

    float getIterations() override {
        return 0;
    }

    float getBailout() override {
        return 0;
    }

    float getPower() override {
        return 0;
    }

    void setIterations(float iterations) override {

    }

    void setBailout(float bailout) override {

    }

    void setPower(float power) override {

    }

    float getScale() override {
        return 0;
    }

    float getThickness() override {
        return 0;
    }

    float getBiais() override {
        return 0;
    }

    void setScale(float scale) override {

    }

    void setThickness(float thickness) override {

    }

    void setBiais(float biais) override {

    }
};

class Mandlebulb : public Object {
public:
    Mandlebulb(glm::vec3 position, float size, float iterations, float bailout, float power) : Object()
    {
        this->position = position;
        this->size = size;
        this->iterations = iterations;
        this->bailout = bailout;
        this->power = power;
    }

    glm::vec3 getPosition() override {
        return position;
    }

    float getSize() override {
        return size;
    }

    int getType() override {
        return 4;
    }

    void setPosition(const glm::vec3 &position) override {
        this->position = position;
    }

    float getIterations() override {
        return iterations;
    }

    float getBailout() override {
        return bailout;
    }

    float getPower() override {
        return power;
    }

    void setIterations(float iterations) override {
        this->iterations = iterations;
    }

    void setBailout(float bailout) override {
        this->bailout = bailout;
    }

    void setPower(float power) override {
        this->power = power;
    }

    float getScale() override {
        return 0;
    }

    float getThickness() override {
        return 0;
    }

    float getBiais() override {
        return 0;
    }

    void setScale(float scale) override {

    }

    void setThickness(float thickness) override {

    }

    void setBiais(float biais) override {

    }
};

class Mandlebox : public Object {
public:
    Mandlebox(glm::vec3 position, float size, float iterations, float scale) : Object()
    {
        this->position = position;
        this->size = size;
        this->iterations = iterations;
        this->scale = scale;
    }

    glm::vec3 getPosition() override {
        return position;
    }

    float getSize() override {
        return size;
    }

    int getType() override {
        return 7;
    }

    void setPosition(const glm::vec3 &position) override {
        this->position = position;
    }

    float getIterations() override {
        return iterations;
    }

    float getBailout() override {
        return bailout;
    }

    float getPower() override {
        return power;
    }

    void setIterations(float iterations) override {
        this->iterations = iterations;
    }

    void setBailout(float bailout) override {
        this->bailout = bailout;
    }

    void setPower(float power) override {
        this->power = power;
    }

    float getScale() override {
        return scale;
    }

    float getThickness() override {
        return 0;
    }

    float getBiais() override {
        return 0;
    }

    void setScale(float scale) override {
        this->scale = scale;
    }

    void setThickness(float thickness) override {

    }

    void setBiais(float biais) override {

    }
};

class Gyroid : public Object {
public:
    Gyroid(glm::vec3 position, float size, float scale, float thickness, float biais) : Object()
    {
        this->position = position;
        this->size = size;
        this->scale = scale;
        this->thickness = thickness;
        this->biais = biais;
    }

    glm::vec3 getPosition() override {
        return position;
    }

    float getSize() override {
        return size;
    }

    int getType() override {
        return 5;
    }

    void setPosition(const glm::vec3 &position) override {
        this->position = position;
    }

    float getIterations() override {
        return 0;
    }

    float getBailout() override {
        return 0;
    }

    float getPower() override {
        return 0;
    }

    void setIterations(float iterations) override {

    }

    void setBailout(float bailout) override {

    }

    void setPower(float power) override {

    }

    float getScale() override {
        return scale;
    }

    float getThickness() override {
        return thickness;
    }

    float getBiais() override {
        return biais;
    }

    void setScale(float scale) override {
        this->scale = scale;
    }

    void setThickness(float thickness) override {
        this->thickness = thickness;
    }

    void setBiais(float biais) override {
        this->biais = biais;
    }
};

class Planets : public Object {
public:
    Planets(glm::vec3 position, float size) : Object()
    {
        this->position = position;
        this->size = size;
    }

    glm::vec3 getPosition() override {
        return position;
    }

    float getSize() override {
        return size;
    }

    int getType() override {
        return 6;
    }

    void setPosition(const glm::vec3 &position) override {
        this->position = position;
    }

    float getIterations() override {
        return 0;
    }

    float getBailout() override {
        return 0;
    }

    float getPower() override {
        return 0;
    }

    void setIterations(float iterations) override {

    }

    void setBailout(float bailout) override {

    }

    void setPower(float power) override {

    }

    float getScale() override {
        return 0;
    }

    float getThickness() override {
        return 0;
    }

    float getBiais() override {
        return 0;
    }

    void setScale(float scale) override {

    }

    void setThickness(float thickness) override {

    }

    void setBiais(float biais) override {

    }
};

class Sponge : public Object {
public:
    Sponge(glm::vec3 position, float size, float iteratons, float scale) : Object()
    {
        this->position = position;
        this->size = size;
        this->iterations = iteratons;
        this->scale = scale;
    }

    glm::vec3 getPosition() override {
        return position;
    }

    float getSize() override {
        return size;
    }

    int getType() override {
        return 8;
    }

    void setPosition(const glm::vec3 &position) override {
        this->position = position;
    }

    float getIterations() override {
        return iterations;
    }

    float getBailout() override {
        return 0;
    }

    float getPower() override {
        return 0;
    }

    void setIterations(float iterations) override {
        this->iterations = iterations;
    }

    void setBailout(float bailout) override {

    }

    void setPower(float power) override {

    }

    float getScale() override {
        return scale;
    }

    float getThickness() override {
        return 0;
    }

    float getBiais() override {
        return 0;
    }

    void setScale(float scale) override {
        this->scale = scale;
    }

    void setThickness(float thickness) override {

    }

    void setBiais(float biais) override {

    }
};



#endif //RAYMARCHING_OBJECT_HPP

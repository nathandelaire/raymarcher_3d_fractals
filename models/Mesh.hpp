//
// Created by natha on 07/04/2021.
//

#pragma once

#include <iostream>
#include <vector>

#include "../shaders/Shader.hpp"
#include "../material/Material.hpp"
#include "Vertex.hpp"
#include "Primitive.hpp"

class Mesh {
public:
    Mesh(Primitive *shape);

    virtual ~Mesh();

    //Updates
    void updateModelMatrix();

    void render(Shader *shader);


private:
    //Same with Primitive class
    void initVAO(Primitive *shape);


    //Update Uniforms (ModelMatrix in GPU shader)
    void updateUniforms(Shader *shader);

    unsigned nbVertices;
    unsigned nbIndices;

    //Positions
    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 scale;


    //Model Matrix (its position in a mat)
    glm::mat4 ModelMatrix;

    //Vertex Array Object
    GLuint VAO;
    //Vertex Buffer Object
    GLuint VBO;
    //Element Buffer Object
    GLuint EBO;
};




//
// Created by natha on 07/04/2021.
//

#include "Primitive.hpp"
#include <iostream>
#include <glm/gtx/string_cast.hpp>


//==================================== PRIMITIVE ==================================//

Primitive::Primitive() {}

Primitive::~Primitive() {

}
void Primitive::set(const Vertex *vertices, const unsigned int nbVertices, const GLuint *indices,
                    const unsigned int nbIndices) {

    this->vertices.clear();
    this->indices.clear();

    for(auto i = 0; i < nbVertices; i++)
        this->vertices.push_back(vertices[i]);
    for(auto i = 0; i < nbIndices; i++)
        this->indices.push_back(indices[i]);
}

Vertex *Primitive::getVertices() {
    return vertices.data();
}

GLuint *Primitive::getIndices() {
    return indices.data();
}

unsigned Primitive::getNbVertices() {
    return vertices.size();
}

unsigned Primitive::getNbIndices() {
    return indices.size();
}

const glm::vec3 &Primitive::getCenter() const {
    return center;
}








//============================================ QUAD ==============================================//

Quad::Quad(glm::vec3 camPos, float nearPlane, glm::mat4 ViewMatrix) : Primitive() {

    glm::vec4 TL = glm::vec4(1.f , 1.0f , - nearPlane - 0.2f, 1.f) * ViewMatrix;
    glm::vec4 BL = glm::vec4(1.f , -1.0f , - nearPlane - 0.2f, 1.f) * ViewMatrix;
    glm::vec4 BR = glm::vec4(-1.f , -1.0f , - nearPlane - 0.2f, 1.f) * ViewMatrix;
    glm::vec4 TR = glm::vec4(-1.f , 1.0f , - nearPlane - 0.2f, 1.f) * ViewMatrix;


    Vertex vertices[] = {
            //Position
            /*0*/ glm::vec3(TL.x + camPos.x, TL.y + camPos.y, TL.z + camPos.z),
            /*1*/ glm::vec3(BL.x + camPos.x, BL.y + camPos.y, BL.z + camPos.z),
            /*2*/ glm::vec3(BR.x + camPos.x, BR.y + camPos.y, BR.z + camPos.z),
            /*3*/ glm::vec3(TR.x + camPos.x, TR.y + camPos.y, TR.z + camPos.z),


    };
    unsigned nrOfVertices = sizeof(vertices) / sizeof(Vertex);

    //Indexes of points that will be used for construction (avoid having two vertex at the same place)
    GLuint indices[] = {
            0, 1, 3, //TR1
            1, 2, 3  //TR2
    };
    unsigned nrOfIndices = sizeof(indices) / sizeof(GLuint);

    set(vertices, nrOfVertices, indices, nrOfIndices);
}



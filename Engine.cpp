//
// Created by natha on 08/04/2021.
//

#include "Engine.hpp"

static int INCR = 0;
static float LIMIT = 43.5f;
static bool SHOULD_INCR = true;

Engine::Engine(const int wWidth, const int wHeight, const char *title, Scene *scene) :
            W_WIDTH(wWidth), W_HEIGHT(wHeight), title(title){

    window = nullptr;
    initGLFW();
    initWindow();
    initGLEW();
    initOpenGLOptions();
    initMatrices();

    //Delta time var init
    dt = 0.f;
    curTime = 0.f;
    lastTime = 0.f,

    //Mouse var init
    lastMouseX = 0.0;
    lastMouseY = 0.0;
    mouseX = 0.0;
    mouseY = 0.0;
    mouseOffsetX = 0.0;
    mouseOffsetY = 0.0;

    firstMouse = true;

    camera = Camera(glm::vec3(0.f, 0.f, 1.f),
                    glm::vec3(0.f, 0.f, 1.f),
                    glm::vec3(0.f, 1.f, 0.f));

    loadedScene = scene;

    initShaders();
    initMaterials();
    initFrame();

    //Load object in json scene we put on parameter if we decided to load one
    if(loadedScene)
        initScene();
    else
        initObjects();


    initLights();
    initUniforms();
}

Engine::~Engine() {
    delete light;
    delete frame;
    delete material;
    delete shader;
    for(auto *o : objects)
        delete o;

    //End of program
    glfwDestroyWindow(window);
    glfwTerminate();
}

void Engine::initGLFW() {
    //Init GLFW
    if(glfwInit() == GLFW_FALSE) {
        std::cout << "ERROR::GLFW_INIT_FAILED" << std::endl;
        glfwTerminate();
    }
}


void Engine::framebuffer_resize_callback(GLFWwindow *window, int fbW, int fbH) {
    glViewport(0, 0, fbW, fbH);
}


void Engine::initWindow() {
    //Version de OpenGL
    //4.4 -> Major = 4, Minor = 4
    const int GLMajor = 4;
    const int GLMinor = 4;

    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, GLMajor);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, GLMinor);
    //Resizable
    glfwWindowHint(GLFW_RESIZABLE, true);

    window = glfwCreateWindow(W_WIDTH, W_HEIGHT, title, NULL, NULL);
    if(!window)
    {
        std::cout << "ERROR::WINDOW_INIT_FAILED" << std::endl;
        glfwTerminate();
    }

    fbwidth = W_WIDTH;
    fbheight = W_HEIGHT;

    //We get H and W of frame in these two variables
    glfwGetFramebufferSize(window, &fbwidth, &fbheight);
    glfwSetFramebufferSizeCallback(window, framebuffer_resize_callback);

    //Put context to main window
    glfwMakeContextCurrent(window);
}

void Engine::initGLEW() {
    //Init GLEW (needs window and openGL context)
    glewExperimental = GL_TRUE;

    //Error if issue
    if(glewInit() != GLEW_OK)
    {
        std::cout << "Glew init failed !" << std::endl;
        glfwTerminate();
    }
}

void Engine::initOpenGLOptions() {
    //OpenGL Options
    glEnable(GL_DEPTH_TEST);
    //Backface culling -> we don't draw what's behind
    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);
    //glFrontFace(GL_CCW); //-> CCW : Counter clockwise
    //Fill shape with color
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    //Enable blending of colors
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //We don't see mouse cursor when we launch app
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void Engine::initMatrices() {
    //Camera vectors
    camPosition = glm::vec3(0.f, 0.f, 1.f);
    camUp = glm::vec3(0.f, 1.f, 0.f);
    camFront = glm::vec3(0.f, 0.f, -1.f);
    //Matrix for View (plan image caméra)
    ViewMatrix = glm::mat4(1.f);
    ViewMatrix = glm::lookAt(camPosition, camPosition + camFront, camUp);

    //Projection matrix : camera view in perspective projection
    fov = 60.f;
    nearPlane = 0.5f; //Dist first plane from cam
    farPlane = 5.f; //Dist last plane from cam (render distance)
    ProjectionMatrix = glm::mat4(1.f);
    ProjectionMatrix = glm::perspective(glm::radians(fov),static_cast<float>(fbwidth) / fbheight,
                                        nearPlane,
                                        farPlane);

}

void Engine::initShaders() {
    //Init Shader
    //Shaders = Program Run in GPU (GPU -> 1000 cores so run multiple small function a/ same time)
    shader = new Shader("shaders/vertex_core.glsl",
                       "shaders/fragment_core.glsl");
}


void Engine::initMaterials() {
    //MATERIAL
    //Main material of objects (light reflection properties)
    material = new Material(glm::vec3(0.6f),
                       glm::vec3(0.8f),
                       glm::vec3(0.7f));
}

void Engine::initFrame() {
    //MODEL
    Quad frame_shape = Quad(camera.getPosition(), nearPlane, glm::mat4(1.f));
    frame = new Mesh(&frame_shape);
}

void Engine::initScene() {

    OBJ_TYPE object = loadedScene->getObject();
    if(object == PLANETS)
    {
        Planets *planets = new Planets(glm::vec3(0.f), 0);
        objects.push_back(planets);
    }
    if(object == MANDELBULB)
    {
        Mandlebulb *mandle = new Mandlebulb(loadedScene->getPosition(),
                                            10,
                                            40,
                                            40,
                                            2);

        objects.push_back(mandle);
    }
    if(object == MANDELBOX)
    {
        Mandlebox *mandelbox = new Mandlebox(loadedScene->getPosition(),
                                             10,
                                             10,
                                             2);

        objects.push_back(mandelbox);
    }
    if(object == GYROID)
    {
        Gyroid *gyroid = new Gyroid(glm::vec3(0.f), 0, 1.f, 1.65f, 3.f);
        objects.push_back(gyroid);
    }
    if(object == SPONGE)
    {
        Sponge *sponge = new Sponge(loadedScene->getPosition(),
                                    10,
                                    6,
                                    3);
        objects.push_back(sponge);
    }

    shader->setVec3f(loadedScene->getColor(), "obj_color");

    shader->set1f(loadedScene->getMaxRaySteps(), "MAX_RAY_STEPS");
    shader->set1f(loadedScene->getMaxDistance(), "MAX_DISTANCE");
    shader->set1f(loadedScene->getMinDistance(), "MIN_DISTANCE");

    shader->set1i(loadedScene->isRenderGlow(), "renderGlow");
    shader->set1i(loadedScene->isRenderAo(), "renderAO");
    shader->set1i(loadedScene->isRenderNormals(), "renderNormals");

    shader->set1i(objects.size(), "NB_OBJ");

}


void Engine::initObjects() {
    Sphere *sphere1 = new Sphere(glm::vec3(-25.f, -10.f, -35.f), 8);
    Cube *cube1 = new Cube(glm::vec3(-5.f, 5.f, -35.f), 6);
    Torus *torus1 = new Torus(glm::vec3(15.f, -5.f, -35.f), 5);
    Octahedron *octa1 = new Octahedron(glm::vec3(25.f, 15.f, -35.f), 6);

    objects.push_back(sphere1);
    objects.push_back(cube1);
    objects.push_back(torus1);
    objects.push_back(octa1);


    Mandlebulb *mandle = new Mandlebulb(glm::vec3(2.f, -1.f, -3.f),
                                        10,
                                        40,
                                        30,
                                        2);

    //objects.push_back(mandle);

    Gyroid *gyroid = new Gyroid(glm::vec3(0.f), 0, 1.f, 1.65f, 3.f);

    //objects.push_back(gyroid);

    Planets *planets = new Planets(glm::vec3(0.f), 0);

    //objects.push_back(planets);

    Mandlebox *mandelbox = new Mandlebox(glm::vec3(10.f, 10.f, -10.f),
                                         10,
                                         10,
                                         2);

    //objects.push_back(mandelbox);

    shader->setVec3f(glm::vec3(1.f), "obj_color");

    shader->set1f(200, "MAX_RAY_STEPS");
    shader->set1f(100, "MAX_DISTANCE");
    shader->set1f(0.01, "MIN_DISTANCE");

    shader->set1i(false, "renderGlow");
    shader->set1i(true, "renderAO");
    shader->set1i(false, "renderNormals");

    shader->set1i(objects.size(), "NB_OBJ");
}


void Engine::initLights() {
    //Lights
    light = new glm::vec3(0.f, 0.f, 2.f);
}

void Engine::initUniforms() {
    //Send Matrices and Light vect to GPU program

    shader->setMatrix4fx(ViewMatrix, "ViewMatrix");
    shader->setMatrix4fx(ProjectionMatrix, "ProjectionMatrix");

    //We need light position for lighting
    shader->setVec3f(*light, "lightPos0");

    //Init Object arrays in the shaders for raymarching
    for(auto i = 0; i < objects.size(); i++)
    {
        shader->setVec3f(objects[i]->getPosition(), ("objects[" + std::to_string(i) + "].center").c_str());
        shader->set1f(objects[i]->getSize(), ("objects[" + std::to_string(i) + "].size").c_str());
        shader->set1i(objects[i]->getType(), ("objects[" + std::to_string(i) + "].type").c_str());

        //Just for mandlebulbs
        shader->set1f(objects[i]->getIterations(), ("objects[" + std::to_string(i) + "].iterations").c_str());
        shader->set1f(objects[i]->getBailout(), ("objects[" + std::to_string(i) + "].bailout").c_str());
        shader->set1f(objects[i]->getPower(), ("objects[" + std::to_string(i) + "].power").c_str());

        //Just for gyroids
        shader->set1f(objects[i]->getScale(), ("objects[" + std::to_string(i) + "].scale").c_str());
        shader->set1f(objects[i]->getThickness(), ("objects[" + std::to_string(i) + "].thickness").c_str());
        shader->set1f(objects[i]->getBiais(), ("objects[" + std::to_string(i) + "].biais").c_str());

    }

}

void Engine::updateKeyboardInput()
{
    //Quit
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);

    //Camera
    if(glfwGetKey(window , GLFW_KEY_E) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, FORWARD);
    if(glfwGetKey(window , GLFW_KEY_S) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, LEFT);
    if(glfwGetKey(window , GLFW_KEY_F) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, RIGHT);
    if(glfwGetKey(window , GLFW_KEY_X) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, BACKWARD);
    if(glfwGetKey(window , GLFW_KEY_SPACE) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, UP);
    if(glfwGetKey(window , GLFW_KEY_C) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, DOWN);

    //Sprint
    if(glfwGetKey(window , GLFW_KEY_W) == GLFW_PRESS)
        camera.setMovementSpeed(15.f);
    else
        camera.setMovementSpeed(3.f);
}

void Engine::updateMouseInput() {
    glfwGetCursorPos(window, &mouseX, &mouseY);

    if(firstMouse)
    {
        lastMouseX = mouseX;
        lastMouseY = mouseY;
        firstMouse = false;
    }

    mouseOffsetX = mouseX - lastMouseX;
    mouseOffsetY = mouseY - lastMouseY;

    lastMouseX = mouseX;
    lastMouseY = mouseY;
}

void Engine::updateDeltaTime() {
    //Get current time
    curTime = static_cast<float>(glfwGetTime());
    //Check update diff
    dt = curTime - lastTime;
    //Update last time
    lastTime = curTime;
}



void Engine::update() {
    //Check update input from mouse and keyboard
    glfwPollEvents();
    updateDeltaTime();
    updateFrame();
    updateObjects();
    updateKeyboardInput();
    updateMouseInput();
    //Update uniforms for GPU shader at each frames
    updateUniforms();
    camera.updateInput(dt, -1, mouseOffsetX, mouseOffsetY);
}

void Engine::updateFrame() {
    delete frame;

    Quad new_frame = Quad(camera.getPosition(), nearPlane, ViewMatrix);
    frame = new Mesh(&new_frame);

}

float RandomFloat(float a, float b) {
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

void Engine::updateObjects() {

    if(loadedScene and loadedScene->isAnimate())
    {
        //FOR MANDLEBULB
        if(loadedScene->getObject() == MANDELBULB) {
            float mandle_var = objects[0]->getPower();
            objects[0]->setPower(mandle_var + 0.005f);
        }
        //FOR MANDELBOX
        if(loadedScene->getObject() == MANDELBOX) {
            float mandleb_var = objects[0]->getScale();
            objects[0]->setScale(mandleb_var + 0.005f);
        }
        //FOR GYRO
        if(loadedScene->getObject() == GYROID) {
            float gyro_var = objects[0]->getThickness();

            if (INCR >= LIMIT) {
                INCR = 0;
                if(SHOULD_INCR)
                    LIMIT = RandomFloat(10,44);
                SHOULD_INCR = !SHOULD_INCR;
            }
            if (SHOULD_INCR) {
                objects[0]->setThickness(gyro_var + 0.009f);
                INCR++;
            } else {
                objects[0]->setThickness(gyro_var - 0.009f);
                INCR++;
            }
        }
        //FOR SPONGE
        if(loadedScene->getObject() == SPONGE)
        {
            float sponge_var = objects[0]->getScale();
            objects[0]->setScale(sponge_var + 0.0005f);
        }
    }

    //Update in shaders
    for(auto i = 0; i < objects.size(); i++)
    {
        shader->setVec3f(objects[i]->getPosition(), ("objects[" + std::to_string(i) + "].center").c_str());
        shader->set1f(objects[i]->getSize(), ("objects[" + std::to_string(i) + "].size").c_str());
        shader->set1i(objects[i]->getType(), ("objects[" + std::to_string(i) + "].type").c_str());

        //Just for mandlebulbs
        shader->set1f(objects[i]->getIterations(), ("objects[" + std::to_string(i) + "].iterations").c_str());
        shader->set1f(objects[i]->getBailout(), ("objects[" + std::to_string(i) + "].bailout").c_str());
        shader->set1f(objects[i]->getPower(), ("objects[" + std::to_string(i) + "].power").c_str());

        //Just for gyroids
        shader->set1f(objects[i]->getScale(), ("objects[" + std::to_string(i) + "].scale").c_str());
        shader->set1f(objects[i]->getThickness(), ("objects[" + std::to_string(i) + "].thickness").c_str());
        shader->set1f(objects[i]->getBiais(), ("objects[" + std::to_string(i) + "].biais").c_str());
    }
}



void Engine::updateUniforms() {

    //Get new width and height from window in case of window size changing
    glfwGetFramebufferSize(window, &fbwidth, &fbheight);

    //Update matrices translations -> Things that change every frame
    ViewMatrix = camera.getViewMatrix();
    ProjectionMatrix = glm::perspective(glm::radians(fov),
                                        static_cast<float>(fbwidth) / fbheight,
                                        nearPlane,
                                        farPlane);
    shader->setMatrix4fx(ProjectionMatrix, "ProjectionMatrix");
    shader->setMatrix4fx(ViewMatrix, "ViewMatrix");

    //We also need to send camera
    shader->setVec3f(camera.getPosition(), "camPosition");
    shader->setVec3f(camera.getFront(), "camFront");
}

void Engine::render() {

    //Clear the window so that we can put new stuff on it
    glClearColor(0.f, 0.f, 0.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    //Draw -> Raymarcher
    frame->render(shader);

    //Update material uniforms
    material->sendToShader(*shader);

    shader->use();

    //End draw -> swap buffers from cleaned old to new
    glfwSwapBuffers(window);
    glFlush();

    //Unbind all stuff
    glBindVertexArray(0);
    glUseProgram(0);
    glActiveTexture(0);
    glBindTexture(GL_TEXTURE_2D, 0);

}

GLFWwindow *Engine::getWindow() const {
    return window;
}


